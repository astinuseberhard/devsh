#!/bin/bash

RUN=$( readlink -nf "$( dirname $0 )/main.sh" )
( [ -f /etc/bash.bashrc ] && grep -q ". \"$RUN\"" /etc/bash.bashrc ) || {
    echo 'Injecting main.sh to /etc/bash.bashrc...'
    echo . \"$RUN\" | sudo tee -a /etc/bash.bashrc
}
#read -n1 -s -p 'Install additionals (php-cli-2-fpm, composer, extra systemd services, etc. )? (y/n)' ; echo ; echo $REPLY | grep -iq y || return
#. main.sh || return
#[ -x $( type -p php ) ] || ( sudo apt update && sudo apt install php-cli php-mysql php-pgsql)
#[ -x $( type -p composer ) ] || sudo update-alternatives --install /usr/bin/composer composer $DEVSH_BASEDIR/bin/composer 10
#sudo systemctl enable $DEVSH_BASEDIR/systemd/*
