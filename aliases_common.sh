#!/bin/bash

# Pause: wstrzymuje działanie skryptów bash, wyświetlając komunikat i czeka na reakcję użytkownika
alias pause='read -n1 -p "Press any key..."'

# PHPUNIT z włączonym xdebug (xdebug.so musi być dodany do środowiska CLI PHP)
alias pu="$PHPUNIT_CMD"

# Poniżej potrzebne by działał plik ~/.pgpass
#alias psql='psql -h localhost'

# Alias do przekierowywania strumienia do schowka X (wymaga xclip)
alias xc='xclip -selection c -f'

# Reset powiadomień o zmianach w pakiecie DEVSH
alias reset_changelog='rm $DEVSH_BASEDIR/changelog/*'

# Open terminal application
alias tt="$TERMINAL_CMD"

alias grepip='grep -oPm 1 "(0|1\d?\d?|2([01234]\d?|5[012345]?|[6789])?|[3456789]\d?)(\.(0|1\d?\d?|2([01234]\d?|5[012345]?|[6789])?|[3456789]\d?)){3}"'

alias genuuid='echo $(genhex 8)-$(genhex 4)-$(gentr 1-4 1)$(genhex 3)-$(gentr 89ab 1)$(genhex 3)-$(genhex 12)'
