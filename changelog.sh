#!/bin/bash

changelog 1.1 "Wersja 1.1:" \
    " - nowy zbiorczy changelog" \
    " - nowa informująca funkcja devsh zwracająca opis i wersję biblioteki oraz listę komend" \
    " - alias lxce_dev zamieniony na przydatną funkcję: lxce_dev <kontener> <użytkownik>"
