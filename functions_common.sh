#!/bin/bash

__clear(){ { unset $1 ; unalias $1 ;} >/dev/null 2>&1 ;}

function block
{
    color=$1 ; shift 1
    ml=0
    for i in "$@" ; do
        [[ $ml -lt ${#i} ]] && ml=${#i}
    done
    echo -ne $color; for ((i=0;$i<$ml+2;i++)); do echo -n ' '; done; echo -e '\e[0m';
    for i in "$@" ; do
        echo -ne "$color"; echo -ne " $i ";
        for ((j=${#i}; $j<$ml; j++)) { echo -n ' ' ;}
        echo -e '\e[0m';
    done
    echo -ne $color; for ((i=0;$i<$ml+2;i++)); do echo -n ' '; done; echo -e '\e[0m';
}

function success
{
    block '\e[1;42m' "$@"
}

function warning
{
    block '\e[1;43m' "$@"
}

function error
{
    block '\e[1;3;41m' "$@"
}

function devsh
{
    local -a list
    for cmd in $(echo ${!DEVSH_CMDS[@]} | sort ) ; do echo $cmd ; done | sort | while read cmd ; do 
        list[${#list[@]}]="${cmd::20} - ${DEVSH_CMDS[$cmd]}"
    done
    success "[DEVSH v$( git -C "$DEVSH_BASEDIR" describe --tags )]" \
        "Zestaw narzędzi developerskich do obsługi projektów zdalnych w środowisku UNIX pod powłoką BASH" \
        "" \
        "Lista komend:" \
        "${list[@]}"
}

function devsh_add_command
{
    cmd=$1 ; shift 1
    DEVSH_CMDS["$cmd"]=$@
}

devsh_add_command block 'nice text block'
devsh_add_command success 'success text block'
devsh_add_command warning 'warning text block'
devsh_add_command error 'error text block'

function changelog
{
    p=$DEVSH_BASEDIR/changelog/$(echo $* | sha1sum | cut -d\  -f1);
    if [[ ! -f $p ]]; then
        touch $p;
        fl="[DEVSH Changelog: $1] $2"; shift 2
        success "$fl" "$@"
        echo -e '\e[2;3;30mTo info pokazuje się tylko raz. Wpisz reset_changelog aby ponownie wyświetlić powiadomienia o zmianach w DEVSH.\e[0m'
    fi;
}

# Uruchamia polecenie w trybie DETACHED z przekierowanym wyjściem do /dev/null w tle, które będzie działać nawet po wyjściu z Bash'a
function @ {
    cmd="$1"
    shift 1
    eval "$cmd" "${@@Q}" >/dev/null 2>&1 & disown -h
}

# Odmontowywanie jeśli zamontowano
function fusermount.umount.ifmounted
{   
    if mountpoint -q "${1}" ; then
        fusermount -u "${1}"
    fi 
}

# dodatkowe informacje
function local_ip {
    ip addr | grep -P '(?<=inet )\d+(\.\d+){3}' | while read -a t ; do
        echo ${t[1]} \(${t[$(( ${#t[*]} - 1 ))]}\)
    done
}

# Rozwiązuje nazwę hosta na IP wg lokalnych regół rozwiązywania nazw DNS
function iplookup
{
    nslookup $@ | awk '/^Address: / { print $2 ; exit }'
}

function ssh_benchmark
{
    host=${1:-localhost}
    count=${2:-100}
    shift 2
    for cipher in $( ssh -Q cipher | shuf ); do
        echo -en "\e[0;93m${cipher}:\e[0m "
        echo $( ssh -c $cipher -q $host "$@" -- dd if=/dev/zero bs=1M count=$count >/dev/null ) | grep -oP '\d+(,\d+)? MB/s' || echo -e '\e[31merror\e[0m'
        echo
    done
}

function generate_project_aliases
{
    name="$1"
    host_local="$2"
    url_local="$host_local:$3"
    dir_local="$MOUNT_DIR/${4:-$1}"
    alias go_$name="$SSH_CMD $host_local -A -X -R 9000:127.0.0.1:9000"
    alias mnt_$name="mkdir -p $dir_local; mountpoint -q $dir_local || $SSHFS_CMD $url_local $dir_local"
    alias umnt_$name="fusermount -u $dir_local"
}

# zmienia nazwy plików. Jeśli nie podano 2 parametru, czyści nazwę pliku z niedozwolonych znaków
mvc(){ r="$2"; [[ -z $r ]] && r="$(echo "$1" | sed -r 's/[^[:alnum:][:space:].-]//g')"; /bin/mv "$1" "$r"; }

# zmienia maskę zapisaną jako "X.X.X.0" na "/X" (np. 255.255.0.0. na /16)
function netmask2lsb() {
    local -a octets
    IFS=. read -r -a octets <<< "$@"
    local i=0 m=31 v=$( printf '%d' "$((${octets[0]} * 256 ** 3 + ${octets[1]} * 256 ** 2 + ${octets[2]} * 256 + ${octets[3]}))")
    while [ $v -ge $((2 ** $i)) ] ; do 
        (($v & ( 2 ** $i ))) || m=$i
        i=$(( $i + 1 ))
    done
    echo $((31 - $m));
}

# Pełna aktualizacja systemu
function sysupgrade() {
    { 
        [ -d /.git ] && local GIT_DIR=/
        [ -d /etc/.git ] && local GIT_DIR=/etc
        { ! [ -v GIT_DIR ] ; } || { sudo git -C $GIT_DIR add -A && sudo git -C $GIT_DIR commit -m "$(uname -a): pre upgrade" ; }
    }
    {
        sudo apt-get -y update \
            && sudo apt-get -y autoremove \
            && sudo apt-get -y autoclean \
            && sudo apt-get -y upgrade \
            && sudo apt-get -y update \
            && sudo apt-get -y dist-upgrade \
            && sudo apt-get -y autoremove \
            && sudo apt-get -y autoclean
    }
    {
        { ! [ -v GIT_DIR ] ; } || { sudo git -C $GIT_DIR add -A && sudo git -C $GIT_DIR commit -m "$(uname -a): post upgrade" ; }
    }
}

# List Usage: pokazuje rozmiar wszystkich plików i katalogów bieżącego katalogu w postaci rosnąco posortowanej listy
function lu() {
    shopt -s dotglob
    local paths args=(-xdev)
    for i in "$@" ; do
        [[ ( ${#args[@]} -eq 1 ) && ( ${i:0:1} != '-' ) ]]\
            && paths[${#paths[@]}]="$i"\
            || args[${#args[@]}]="$i"
    done
    find "${paths[@]}" "${args[@]}" | while read path ; do
        du -sxh "$path" 2>/dev/null
    done | sort -h
}

x(){ [ $# -eq 0 ] && set . ; @ xdg-open "$@" ; }

locate_project_file(){
    local md=2
    local t=composer.json
    
    [ -n "$1" ] && t="$1"
    [ -n "$2" ] && md="$2"
    [ -n "$3" ] && local l=$( readlink -f "$3" )

    local cmd="find"
    [ -v l ] && cmd="$cmd $l"
    cmd="$cmd -maxdepth $md -name $t ${@:4}"

    # echo $cmd >&2
    $cmd | head -n 1
}

scan_projects () 
{ 
    local C='\e[0m' OPTIND=1;
    local -x status;
    function show_usage () 
    { 
        [ $# -gt 0 ] && echo "$@";
        echo "Usage: scan_projects [-f] [-a] <path> ... "
    };
    function debug () 
    { 
        [ -v debug ] && echo "$@" 1>&2;
        return 0
    };
    shopt -s dotglob;
    [ -z "$1" ] && set $(ls -A);
    for i in "$@";
    do
        [ -d "$i" ] || continue;
        [ -d "$i/.git" ] && { 
            pushd $i > /dev/null;
            [ -v FETCH ] && git fetch -q;
            local changes=$(git status -s 2>/dev/null | wc -l);
            lastChange=$(git reflog --date=relative -n1 2>/dev/null | grep -oP '(?<=HEAD@{).* ago(?=}:)');
            branch=$(git branch 2>/dev/null | grep -oP "(?<=^\* )[^\(].*$" || git describe --always --all 2>/dev/null );
            [ "$branch" ] && local status="$branch${lastChange:+ - $lastChange}";
            popd > /dev/null
        };
        local composer_source=$(locate_project_file composer.json 2 $i);
        local desc="";
        [ "$composer_source" ] && { 
            function find_version () 
            { 
                grep -oP "(?<=$1)\s+.*?\s+" | xargs
            };
            local composer_info=$($COMPOSER_CMD --working-dir="$(dirname "$composer_source")" licenses );
            local name=$(echo "$composer_info" | grep -oP '(?<=Name: ).*$');
            [ "$name" == '__root__' ] && unset name && continue;
            local version=$(echo "$composer_info" | grep -oP '(?<=Version: ).*$' | grep -v 'No version set' );
            local symfony=$(echo "$composer_info" | find_version domenypl/symfony1-legacy );
            [ "$symfony" ] || symfony=$(echo "$composer_info" | find_version symfony/symfony );
            local doctrine=$(echo "$composer_info" | find_version doctrine/orm );
            local symfony_console=$(echo "$composer_info" | find_version symfony/console );
            local phpunit=$(echo "$composer_info" | find_version phpunit/phpunit );
            local api=$(echo "$composer_info" | find_version api-platform/core );
            local domeny_lib=$(echo "$composer_info" | find_version domeny/lib );
            [ "$symfony" ] && desc="$desc\e[2;33m[Symfony: \e[2;93m$symfony\e[2;33m]$C";
            [ "$doctrine" ] && desc="$desc\e[2;35m[Doctrine: \e[2;95m$doctrine\e[2;35m]$C";
            [ "$symfony_console" ] && desc="$desc\e[2;31m[console: \e[2;91m$symfony_console\e[2;31m]$C";
            [ "$phpunit" ] && desc="$desc\e[2;34m[PHPUnit: \e[2;94m$phpunit\e[2;34m]$C";
            [ "$api" ] && desc="$desc\e[2;32m[API-platform: \e[2;92m$api\e[2;32m]$C";
            [ "$domeny_lib" ] && desc="$desc\e[0;36m[Domeny.pl: \e[0;96m$domeny_lib\e[2;36m]$C";
            local PHP='[PHP]'
        };
        local package_source=$(locate_project_file package.json 2 $i);
        [ "$package_source" ] && {
            local -A package_info=\($( cat "$package_source" | grep -P '^\s*".*":\s*".*",?$' | sed -r 's/^\s*"(.*?)":\s*"(.*?)"\s*,?$/[\1]="\2"/g' | xargs -0 )\);
            local name="${package_info[name]}$"
            [ "${package_info[version]}" ] && local version="${package_info[version]}";
            [ "${package_info[babel-core]}" ] && desc="$desc\e[2;31m[Babel: \e[2;91m${package_info[babel-core]}\e[2;31m]$C";
            [ "${package_info[eslint]}" ] && desc="$desc\e[2;35m[ESLint: \e[2;95m${package_info[eslint]}\e[2;35m]$C";
            [ "${package_info[react]}" ] && desc="$desc\e[2;32m[React: \e[2;92m${package_info[react]}\e[2;32m]$C";
            [ "${package_info[webpack]}" ] && desc="$desc\e[2;36m[WebPack: \e[2;96m${package_info[webpack]}\e[2;36m]$C";
            [ "${package_info[electron]}" ] && desc="$desc\e[2;33m[Electron: \e[2;93m${package_info[electron]}\e[2;33m]$C";
            local NPM='[NPM]'
        };
        ( [ -v ALL ] || [ -v changes ] ) && {
            local prefix=""
            echo -ne "$( printf "\e[1;91m%5s$C\e[1;95m%5s$C " "$PHP" "$NPM" )"
            printf "\e[1m%-30s$C \e[1;93m%-20s$C " "./$i" "$name"
            printf "%-15s " "$version";
            [ $changes -eq 0 ] && echo -ne "\e[7;32m clean $C" || echo -ne "\e[7;31m dirty $C"
            printf "   \e[2m%-40s$C" "$status"
            echo -e "$desc"
            unset name version desc status changes PHP NPM
        }
    done
}

scan_projects2(){
    local C='\e[0m' OPTIND=1
    local -x status

    show_usage(){ [ $# -gt 0 ] && echo "$@" ; echo "Usage: scan_projects [-f] [-a] <path> ... " ; }
    debug(){ [ -v debug ] && echo "$@" ; return 0 ; }

    # while getopts fah\? OPT ; do case "$OPT" in
    #     f) declare FETCH= ;;
    #     a) declare ALL= ;;
    #     h|H) declare show_usage && return 0 ;;
    #     ?) show_usage "Invalid argument specyfied: $OPTARG" && return 1 ;;
    #     *) show_usage "Invalid argument specyfied: $OPT" && return 1 ;;
    # esac ; done ; shift $(( OPTIND - 1 ))

    shopt -s dotglob
    # echo zdghds
    [ -z "$1" ] && set $(ls -A)
    # echo zdghdsafgags
    # echo $@ ; return
    for i in "$@" ; do
        # echo sgsg
        [ -d "$i" ] || continue

        # echo lkkkasag
        [ -d "$i/.git" ] && {
            pushd $i >/dev/null
            [ -v FETCH ] && git fetch -q
            fileCount=$(git status -s 2>/dev/null | wc -l)
            lastChange=$(git reflog --date=relative -n1 2>/dev/null | grep -oP '(?<=HEAD@{).* ago(?=}:)')
            branch=$(git branch 2>/dev/null | grep -oP "(?<=^\* )[^\(].*$" || git describe --always --all 2>/dev/null )
            [ "$branch" ] && local status="\e[2m$branch${lastChange:+ - $lastChange}$C"
            [[ $branch == 'master' ]] && {
                [ $fileCount -eq 0 ] \
                    && status="$status \e[7;32m free $C" \
                    || status="$status \e[7;31m busy $C"
            }
            popd >/dev/null
        }

        # PHP: Composer
        local composer_source=$(locate_project_file composer.json 2 $i)
        # echo achlaca
        [ "$composer_source" ] && {
            find_version(){
                grep -oP "(?<=$1)\s+.*?\s+" | xargs
            }
            local composer_info=$($COMPOSER_CMD --working-dir="$(dirname "$composer_source")" licenses )
            local name=$(echo "$composer_info" | grep -oP '(?<=Name: ).*$')
            local version=$(echo "$composer_info" | grep -oP '(?<=Version: ).*$' | grep -v 'No version set' )
            local symfony=$(echo "$composer_info" | find_version domenypl/symfony1-legacy )
            [ "$symfony" ] || symfony=$(echo "$composer_info" | find_version symfony/symfony )
            local doctrine=$(echo "$composer_info" | find_version doctrine/orm )
            local symfony_console=$(echo "$composer_info" | find_version symfony/console )
            local phpunit=$(echo "$composer_info" | find_version phpunit/phpunit )
            local api=$(echo "$composer_info" | find_version api-platform/core )
            local domeny_lib=$(echo "$composer_info" | find_version domeny/lib )

            local desc="\e[1;93m$name$C:"
            [ "$version" ] && desc="$desc ($version)$C"
            [ "$symfony" ] && desc="$desc \e[2;33m[Symfony: \e[2;93m$symfony\e[2;33m]$C"
            [ "$doctrine" ] && desc="$desc \e[2;35m[Doctrine: \e[2;95m$doctrine\e[2;35m]$C"
            [ "$symfony_console" ] && desc="$desc \e[2;31m[console: \e[2;91m$symfony_console\e[2;31m]$C"
            [ "$phpunit" ] && desc="$desc \e[2;34m[PHPUnit: \e[2;94m$phpunit\e[2;34m]$C"
            [ "$api" ] && desc="$desc \e[2;32m[API-platform: \e[2;92m$api\e[2;32m]$C"
            [ "$domeny_lib" ] && desc="$desc \e[0;36m[Domeny.pl: \e[0;96m$domeny_lib\e[2;36m]$C"

            echo -ne "\e[1m$i$C:!!\e[1;94m[PHP]$C!!($desc)!!"
            [ "$status" ] && echo -ne "- $status"
            echo
            continue
        }

        local package_source=$(locate_project_file package.json 2 $i)
        [ "$package_source" ] && {
            # Arrays are assigned to using compound assignments of the form name=(value1 ... valuen), where each value is of the form [subscript]=string.
            local -A package_info=\($( cat "$package_source" | grep -P '^\s*".*":\s*".*",?$' | sed -r 's/^\s*"(.*?)":\s*"(.*?)"\s*,?$/[\1]="\2"/g' | xargs -0 )\)
            # ( for i in "${!package_info[@]}" ; do echo "$i: ${package_info[$i]}" ; done ) | sort
            # echo ok
            # for i in "${!package_info[@]}" ; do echo $i ; done

            # local version=$( echo "$header" | grep -oP "(?<=@).*(?=\s$(pwd))" )
            local desc="\e[1;93m${package_info[name]}$C:"
                [ "${package_info[version]}" ] && desc="$desc (${package_info[version]})$C"
                [ "${package_info[babel-core]}" ] && desc="$desc \e[2;31m[Babel: \e[2;91m${package_info[babel-core]}\e[2;31m]$C"
                [ "${package_info[eslint]}" ] && desc="$desc \e[2;35m[ESLint: \e[2;95m${package_info[eslint]}\e[2;35m]$C"
                [ "${package_info[react]}" ] && desc="$desc \e[2;32m[React: \e[2;92m${package_info[react]}\e[2;32m]$C"
                [ "${package_info[webpack]}" ] && desc="$desc \e[2;36m[WebPack: \e[2;96m${package_info[webpack]}\e[2;36m]$C"
                [ "${package_info[electron]}" ] && desc="$desc \e[2;33m[Electron: \e[2;93m${package_info[electron]}\e[2;33m]$C"

            echo -ne "\e[1m$i$C:!!\e[1;95m[NPM]$C!!($desc)!!"
            [ "$status" ] && echo -ne "- $status"
            echo
            continue
        }
        
        [ -v ALL ] && echo $i
    done \
        | column -ts '!!'
}

pst(){ pgrep "$@" | sed 's/.*/echo -n "\0: " ; pstree -pusH\0 \0 \| head -n1/' | bash ; }

rp(){ while [ ! ] ; do echo -n "$(date): " ; eval "$@" | head -n1 ; sleep 1 ; done ; }

rpn(){ while [ ! ] ; do out=$( eval "$@" ) ; clear ; echo -e "$(date):\n$out" ;  sleep 5 ; done ; }

# Password generating
gentr() { </dev/urandom tr -dc ${1} | head -c${2:-32} ; [ -t 1 ] && echo ; }
genhex() { gentr 0-9a-f ${1:-32} ; }
genpas() { gentr [:alnum:] ${1:-40} ; }
genpas2() { gentr [:alnum:][:punct:] ${1:-40} ; }

debug(){ [ -v debug ] && echo "$@" ; return 0 ; }

cless(){ "$@" 2>&1 | less ; }

ssh () {
    [ -v SSH_AUTH_SOCK ] || {
        SSH_AUTH_SOCK=$( ss -xl | grep -oP '/\w+/ssh-\w+/agent\.\d+' )
        [ -r $SSH_AUTH_SOCK ] && export SSH_AUTH_SOCK || . <(ssh-agent)
        [ -d ~/.ssh ] && ( read -N 1 -t 5 -p 'Add all keys under ~/.ssh/ ? (y/n)' r ; [[ $r = 'y' ]] ) && {
            ls ~/.ssh/*.pub | while read pubkey ; do
                echo -n | ssh-add ${pubkey%.pub}
            done
        }
    }

    $SSH_CMD "$@"
}

export -f block success warning error changelog fusermount.umount.ifmounted generate_project_aliases netmask2lsb lu x scan_projects local_ip iplookup locate_project_file pst rp rpn gentr genhex genpas genpas2 debug cless
