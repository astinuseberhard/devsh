#!/bin/bash

# Ubuntu Forums Thread: Bash completion of aliased commands
# https://ubuntuforums.org/showthread.php?t=733397
function make-completion-wrapper () {
	local function_name="$2"
	local arg_count=$(($#-3))
	local comp_function_name="$1"
	shift 2
	local function="
function $function_name {
    ((COMP_CWORD+=$arg_count))
    COMP_WORDS=( "$@" \${COMP_WORDS[@]:1} )
    "$comp_function_name"
    return 0
}"
	eval "$function"
	echo $function_name
	echo "$function"
}

# own completion wrapper made by git command completion code from locally installed package
function git-completion-wrapper() { 
	if ( ! declare -f _git_complete > /dev/null ); then
		[ -r /usr/share/bash-completion/completions/git ] && . /usr/share/bash-completion/completions/git
	fi

	if ( ! declare -f __git_complete > /dev/null ); then
		__git_func_wrap() {
			local cur words cword prev
			_get_comp_words_by_ref -n =: cur words cword prev
			$1
		}
	fi

    # Setup completion for certain functions defined above by setting common
    # variables and workarounds.
    # This is NOT a public function; use at your own risk.
	local wrapper="__git_wrap${2}"
	eval "$wrapper () { __git_func_wrap $2 ; }"
	complete -o bashdefault -o default -o nospace -F $wrapper $1 2>/dev/null \
		|| complete -o default -o nospace -F $wrapper $1
}

complete -o bashdefault -c @ 2>/dev/null

complete -A hostname iplookup 2>/dev/null

complete -A hostname iplookup 2>/dev/null

__pst_complete() { COMPREPLY=($(ps -Ao comm | grep -P "^$2" | sort | uniq )) ; }

complete -F __pst_complete pst



export -f make-completion-wrapper git-completion-wrapper