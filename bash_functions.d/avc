#!/bin/bash

avc(){
    unset format onlyAudio config video_config audio_config size dry tune begin end nogpu mkpass quality vbr mbr abr nosub extras inbase outbase wait
    OPTIND=0
    abr=128k
    extras=
    tune=hq
    maps=0

    run(){
        [ -v dry ] && echo "$@" || eval "$@"
    }

    fail(){
        [ $# -gt 0 ] && echo -e "\e[1;31m$@\e[0m"
        cat <<USAGE
Usage: avc [-n] -f <format> [-G] [-P] [-s <size>] [-b <start_sec>] [-e <stop_sec>] [-c <quality> | -v <vbr>] [-m <mbr>] [-a <abr>] [-t <tune>] [-o <extras>]

Parameters:
  -n    Dry run
  -i    Mapping (eq. 0 for -map 0)
  -f    Format (mp3 for audio or mp4 for video)
  -G    No GPU encoding acceleration (mp4 only)
  -P    Performs 2-pass encoding
  -s    Size (eg.: hd480, 852:480, -1:720, etc.; will be used as value to the scale/scale_npp filter, along with some defaults options for that, default is the very same size as origin)
  -b    Start point (in seconds)
  -e    End point (length, in seconds)
  -q    Quality (used as quality value, 0 - lossless, 17-19 - optimised, 63 max, less is better quality but longer encoding and bigger output)
  -v    Variable average bit rate (use along with -P; in opposition to quality, SI units accepted, eg.: 1M)
  -m    Maximum bit rate (SI units accepted, eg.: 4M)
  -M    Buffer size (SI units accepted, should be twice that maximum bitrate, eg.: 8M)
  -a    Audio bit rate (SI units accepted, default 160k)
  -t    Tune (eg.: film, hq, animation, stillimage, zerolatency, etc. - on with GPU disabled)
  -o    Extras (added as is at the end of ffmpeg command)
  -w    Pause for argument in seconds after each finished task
USAGE
        return -1;
    }
    
    while getopts hf:na:q:GPi:s:t:v:b:e:o:m:M:w: OPT
    do
        case "$OPT" in
            G) nogpu= ;;
            i) maps=$OPTARG ;;
            P) mkpass= ;;
            a) abr=$OPTARG ;;
            b) begin=$OPTARG ;;
            q) quality=$OPTARG ;;
            e) end=$OPTARG ;;
            f) format=$OPTARG ;;
            n) dry=1 ;;
            o) extras=$OPTARG ;;
            s) size=$OPTARG ;;
            t) tune=$OPTARG ;;
            v) vbr=$OPTARG ;;
            m) mbr=$OPTARG ;;
            M) buffs=$OPTARG ;;
            w) wait=$OPTARG ;;
            h|H|?) fail ; return 0 ;;
        esac
    done
    shift $(( $OPTIND - 1 ))
    
    [ $# -gt 0 ] || fail 'Nie podano ścieżki do pliku!'
    [ -v format ] || fail 'Nie podano formatu docelowego!'
    # [ -v nogpu ] && [ $tune == 'hq' ] && tune=film
    [ $format != 'mp3' ] || onlyAudio=1

    config=""
    [ -v begin ] && config="$config -ss $begin"
    [ -v end ] && config="$config -t $end"

    if [ -v onlyAudio ] ; then
        video_config="-vn"
        audio_config="libmp3lame"
    else
        [ -v mkpass ] && video_config="-preset slow" || video_config="-preset medium"
        video_config="$video_config -bf 4"
        if [ -v nogpu ] ; then
            video_config="-c:v libx264 -profile:v high -tune $tune $video_config"
            if [ -v size ] ; then
                video_config="$video_config -vf scale=$size:force_original_aspect_ratio=decrease:force_divisible_by=2:out_color_matrix=bt709:out_range=tv"
            fi
        else
            video_config="-c:v h264_nvenc -profile:v high -tune hq $video_config -rc-lookahead 64 -spatial-aq 1 -temporal-aq 1 -b_ref_mode 2 -nonref_p 1"
            if [ -v size ] ; then
                video_config="${video_config} -vf scale=out_color_matrix=bt709:out_range=tv,format=yuv420p,hwupload_cuda,scale_npp=$size:force_original_aspect_ratio=decrease:force_divisible_by=2:interp_algo=super,hwdownload"
            fi
        fi
        video_config="$video_config -rc:v vbr"
        [ -v vbr ] && video_config="$video_config -b:v $vbr"
        [ -v mbr ] && video_config="$video_config -maxrate:v $mbr"
        [ -v buffs ] && video_config="$video_config -bufsize: $buffs"
        video_config="$video_config -qmin 1 -qmax 51 -movflags disable_chpl+faststart -framerate 30 -g 60"
        [ -v mkpass ] && video_config="$video_config -2pass 1"
        audio_config="libfdk_aac"
    fi
    audio_config="-c:a $audio_config -b:a $abr"
    if [ -v onlyAudio ] ; then
        audio_config="-af loudnorm,silencedetect=noise=0.0001 $audio_config"
    fi

    for param ; do
        [ -v inbase ] && [ -v wait ] && [ $wait -gt 0 ] && sleep $wait
        [ -r "$param" ] || fail "Nie można otworzyć pliku '$param'!"
        inbase=$( basename "$param" | grep -oP '^.+(?=\.\S{3,}$)' )
        if [ -v onlyAudio ] ; then
            curconfig="-i \"$param\" -map 0"
        else
            curconfig="-i \"$param\" -map $maps"
        fi
        outbase="${inbase:0:54}"
        [ -v ss ] && outbase="$outbase b$ss"
        [ -v t ] && outbase="$outbase e$t"
        [ -v onlyAudio ] || {
            [ -v tune ] && outbase="$outbase $tune"
            [ -v size ] && outbase="$outbase ${size/:/x}"
            [ -v mkpass ] && outbase="$outbase P"
            [ -v vbr ] && outbase="$outbase $vbr"
            [ -v quality ] && outbase="$outbase q$quality"
        }
        outbase="$outbase $abr $( gentr 0-9a-f 8 )"

        pass=""
        echo -e "\e[1;32m\"$param\":\e[0m"
        if [ ! -v onlyAudio ] ; then
            if [ -v mkpass ] ; then
                run nice ffmpeg -vsync 0 -y $curconfig $video_config -multipass 1 -an $config -pass 1 $extras -f null /dev/null 
                pass="-pass 2"
            fi
        fi
        [[ -v quality && ! -v vbr ]] && video_config="$video_config -cq:v $quality"

        run nice ffmpeg -vsync 0 -y -flags +global_header -fix_sub_duration $curconfig $video_config $audio_config $config $pass $extras -f $format \"$outbase.$format\"

    done
}

mp4_fix(){
    ffmpeg -i "$1" -map 0:v:0 -map 0:a:0 -c copy -movflags disable_chpl+faststart "${1%mp4}fix.mp4" && rm "$1" && mv "${1%mp4}fix.mp4" "$1"
}

export -f avc mp4_fix
