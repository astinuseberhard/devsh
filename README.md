### KONFIGURACJA:

* Skonfigurować plik conf.sh

### INSTALACJA AUTOMATYCZNA:

* wykonać skrypt install.sh w katalogu głównym projektu
* skrypt przeszuka plik /etc/bash.bashrc w poszukiwaniu wpisu instalacyjnego, jeśli go nie znajdzie to go doda
* zobacz plik install.sh przed wykonaniem

### INSTALACJA RĘCZNA:

* Dodać WYWOŁANIE pliku main.sh do ~/.bashrc:

```bash
    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

    . ~/<sciezka do devsh>/main.sh
```
