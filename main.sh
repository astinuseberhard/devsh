#!/bin/bash

export DEVSH_BASEDIR=$(dirname "$( readlink -nf "$BASH_SOURCE" )" );
pushd "$DEVSH_BASEDIR" >/dev/null 2>&1

# DEVSH command list array ([cmd]=desc)
declare -Ax DEVSH_CMDS

# Ładowanie konfiguracji
[ -r ./conf.sh ] || (
    echo -e "\e[33mBrak pliku ./conf.sh - kopiowanie podstawowej zawartości z conf.sh.dist\e[0m"
    /bin/cp conf.sh.dist conf.sh
)
. ./conf.sh

# Wczytanie dodatkowych funkcji
. functions_common.sh
[ -d ./bash_functions.d ] && for i in ./bash_functions.d/* ; do . $i ; done

# Wczytanie aliasów
. aliases_common.sh
[ -d ./bash_aliases.d ] && for i in ./bash_aliases.d/* ; do . $i ; done

# Wczytanie podpowiadań
[ "$( type -t __gitcomp )" == "function" ] && (
    [ -f /usr/share/bash-completion/completions/git ] && . /usr/share/bash-completion/completions/git
    [ -f /etc/bash_completion.d/git ] && . /etc/bash_completion.d/git
)
. completion_common.sh
[ -d ./bash_completion.d ] && for i in ./bash_completion.d/* ; do . $i ; done

# Ustawienie aliasów do skryptów wewnątrz ./bin
export PATH="$PATH:$DEVSH_BASEDIR/bin"

# Finalizacja
[ -r local.sh ] || (
    echo -e "\e[32mBrak pliku ./local.sh - tworzę pusty plik do wprowadzania własnych rozszerzeń\e[0m"
    touch local.sh
)
. local.sh

# Uruchomienie powiadomień o zmianach
. ./changelog.sh

popd >/dev/null 2>&1 || cd $HOME
